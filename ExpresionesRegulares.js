/* Tarea: Crear expresiones regulares para las siguientes condiciones: 
   1) Contraseña simple ->  Numeros y letras.
   2) Contaseña media -> Numeros, letras y al menos una mayuscula.
   3) Contraseña alta -> Numeros, letras, al menos una mayuscula y caracter especial
*/

// 1) Contraseña simple ->  Numeros y letras:
function verificarPassword(pass){
   let exp1 = /^[a-z0-9]{4,9}$/; //Nivel simple(Numeros y letras);
   let exp2 = /^[a-zA-Z0-9]{4,9}$/; //Nivel medio(Numeros, letras y al menos una mayuscula);
   let exp3 = /^[a-zA-Z0-9\W]{4,9}$/; //Nivel medio(Numeros, letras, al menos una mayuscula y caracter especial);

   if(exp1.test(pass)){
      console.log(`   ✅ Nivel simple: ${pass}`);
   }else 
      if(exp2.test(pass)){
         console.log(`   ✅ Nivel medio: ${pass}`);
      }else
         if(exp3.test(pass)){
            console.log(`   ✅ Nivel Alto: ${pass}`);
         }
}

// Capturamos la contraseña del usuario:
let password1 = process.argv[2];

console.log(' - Bienvenidos, debe generar una contraseña con un minimo de 4 caracteres -')

if(password1.length >= 4){
   console.log('✅ Contraseña Aprobada. Nivel de seguridad: ');
   verificarPassword(password1);
}else{
   console.log(`❌ Error, la contraseña debe tener un minimo de 4 caracteres, su contraseña tiene ${password1.length}. Intente nuevamente`);
}






// 2) Contraseña simple ->  Contaseña media -> Numeros, letras y al menos una mayuscula.



