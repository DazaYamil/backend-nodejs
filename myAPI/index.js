//1er paso, instalar express.

const express = require('express');
const app = express();
const puerto = 5050;
const usersRouter = require('./rutas/usersRouter');
const productsRouter = require('./rutas/productsRouter');

// Configuración de rutas, middleware y otras funcionalidades aquí
app.use(express.json()); //Para que express sepa que vamos a usar archivos en formato json en el body
app.use('/myApi',usersRouter); //Estamos usando router express
app.use('/myApi/products',productsRouter); //Estamos usando router express

app.listen(puerto, () => {
   console.log(`¡Zarpamos desde el puerto ${puerto}! Accede a : localhost:5050`);
});