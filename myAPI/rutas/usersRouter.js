// Configuraciones
const express = require('express');
const usersRouter = express.Router();

const users = [];

// Rutas
usersRouter.get('/', (req, res) => {
   let greeting = `
      Hello world!
      Welcome to System Software.
      Let's Go...
   `
   res.send(greeting);
});

//* 1er forma de obtener los datos del frontend en el backend - Esta ruta de tipo GET me muestra todos los usuarios
usersRouter.get('/users', (req, res) => {
   try {
      // const users = usersController.userShow()
      res.status(200).json({
         mensaje: "Te devuelvo todo los users",
         users: users
      })
   } catch (error) {
      res.status(500).json({
         mensaje: "error inesperado"
      })
   }
});

//* 2da Forma de obtener datos del frontend en el backend por medio de un comodin o parametro :parametro
//* Esta ruta nos muestra un usuario en particular pasandole un parametro por el endpoint
usersRouter.get('/users/:id', (req, res) => {
   try {
      //Ni bien recibimos el indice o pos hacemos validaciones
      let idUser = req.params.pos - 1; // Los parametros o comodines nos llega en req.params.comodin
      console.log('ID: ',idUser);
      //Validacion
      if(idUser >= 0 && idUser < users.length){
         let user = users[idUser];
         res.status(200).json({
            message: "User encontrado: " + user.name ,
            fullInfoUser: user
         })
      }else{
         throw new Error(`Usuario con id ${idUser} no existente...`)
      }
   } catch (error) {
      res.status(500).json({
         mensaje: "error inesperado",
         msjError: error.message,
      })
   }
});

usersRouter.post('/users', (req, res) => {
   // Aca van todas las validaciones al recibir la data antes de mostrar o trabajar en ella
   // console.log(req.body);
   let user = req.body;
   users.push(user);
   
   res.status(201).json({
      message:"The user " + user.name + " ha sido creado",
      fullInfoUser: user  
   })
})

//* Ruta para actualizar un usuario. Necesito 2 cosas: que usuario quiero actualizar y QUE le quiero actualizar
usersRouter.put('/users/:pos', (req, res) => {
   let posUser = req.params.pos - 1;
   let beforeUser = users[posUser];
   //Actualizo
   users[posUser] = req.body;
   let afterUser = users[posUser];

   res.status(201).json({
      mensaje:"El user " + beforeUser.name + " ha sido actualizado",
      newUser: afterUser,
   })
})

usersRouter.delete('/users/:pos', (req, res) => {
   let posUser = req.params.pos - 1;

   //Eliminar un usuario en particular:
   let [ userPop ] = users.splice(posUser, 1);
   console.log(userPop);

   res.status(201).json({
      mensaje:"The user " + userPop.name + " ha sido eliminado correctamente...",
   })
})

module.exports = usersRouter;