const express = require('express');
const productsRouter = express.Router();

const products = [];

//GET produts - Devolver informacion al cliente
productsRouter.get('/', (req, res) => {
   try {
      res.status(200).json({
         message: "The show all products",
         products: products,
      })
   } catch (error) {
      res.status(500).json({
         mensaje: "error inesperado"
      })
   }
})

//GET one produt
productsRouter.get('/:id', (req, res) => {
   try {
      let idProduct = req.params.id - 1;

      //Validacion:
      if(idProduct >= 0 && idProduct < products.length){
         res.status(200).json({
            message: "The product " + idProduct,
            dataProduct: products[idProduct],
         })
      }else{
         throw new Error(`Product witch ID ${idProduct + 1} not existyn`)
      }

   } catch (error) {
      res.status(500).json({
         message: "error inesperado",
         error: error.message
      })
   }
})

//POST one products - Crear un recurso en el backend
productsRouter.post('/', (req, res) => {
   try {
      let data = req.body;
      products.push(data);
      res.status(201).json({
         message: "The product create with exito",
         products: data,
      })
   } catch (error) {
      res.status(500).json({
         message: "error inesperado"
      })
   }
})

//PUT one products ID - Actualizar informacion del backend
productsRouter.put('/:id', (req, res) => {
   try {
      let id = req.params.id - 1;
      
      //Validacion:
      if(id >= 0 && id < products.length){
         let data = req.body;
         products[id] = data;

         res.status(201).json({
            message:"The product is actualize",
         })
      }else{
         throw new Error(`The ID ${id + 1} not find`)
      }
   } catch (error) {
      res.status(500).json({
         message: "error inesperado"
      })
   }
})

//PUT one products pos

// DELETE - Eliminar datos del backend

module.exports = productsRouter;