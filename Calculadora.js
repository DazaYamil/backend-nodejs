/* TAREA: Crear un archivo calculadora.js y soporte 1 parámetro de texto. El programa debe distinguir que operación aritmética quiere hacer el usuario y responder
Como parte de la consigna es delimitar el alcance de la calculadora. Importante: La calculadora no debe dar errores en ninguna circunstancia, si hay alguna operación que no soporte debe decirlo
- Examples:
   * node calculadora.js "me gustaría sumar 5 y 6"
   * node calculadora.js "me sumas 5 con 6"
   * node calculadora.js "tengo que hacer una suma de 5 y 6, cuanto me da?"
   * node calculadora.js "me dividis 9 con 3" 
*/

function procesarValores(op, num1, num2){
   console.log(`Su operacion es ${op} entre ${num1} ${operaciones[op]} ${num2}. Su Resultado es: ${num1 + num2}`);
}

const operaciones = {
   sumar: '+',
   restar: '-',
   multiplicar: '*',
   dividir: '/'
}

const EXP_OPERACIONES = /(sum.\S)|(res.)|(divi.)|(mult.)/ig;
const EXP_NUMEROS = /[\.\d]+(?!\.)/g;

let textoUsuario = process.argv[2]; //Texto ingresado por el usuario
const operacion = textoUsuario.match(EXP_OPERACIONES);
console.log(operacion);
const arrNumeros = textoUsuario.match(EXP_NUMEROS);

console.log('-----------------------------   Bienvenido a nuestra calculadora   -----------------------------');
console.log('------- Puedes indicarnos que operacion deseas realizar. Example "Me gustaria sumar 2 y 3" ----------');
console.log();

if(!(EXP_OPERACIONES.test(textoUsuario))){
   console.log(`❌ Error, operación no encontrada ("sumar", "restar", "multiplicar" y/o "dividir") . Texto ingresado: ${textoUsuario}`);
   process.exit(1);
}else 
   if(!(arrNumeros.length >= 2)){
      console.log(`❌ Error, me diseñaron para operar con 2 valores numeros, no encontramos dicha condicion. Texto ingresado: ${textoUsuario}`);
      process.exit(1);
   }
else{
   console.log('✅ Texto correcto, a operar....')
   procesarValores(operacion, Number(arrNumeros[0]), Number(arrNumeros[1]));
} 