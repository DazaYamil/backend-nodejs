import { consola } from 'consola';
import promptSync from 'prompt-sync';
import controller from './controllers/operations.js';
const prompt = promptSync();

const ask = async () => await consola.prompt('Desea realizar operaciones basicas?', {type: 'confirm',}); 

function menu(){
   consola.info(`
      1- Sumar
      2- Restar
      3- Multiplicar
      4- Dividir
   `)
   let option = Number(prompt('Digite que operación desea realizar (1-4): '));
   let num1 = Number(prompt('Ingrese el 1er valor numerico: '));
   let num2 = Number(prompt('Ingrese el 2do valor numerico: '));
   controller(option, num1, num2);
}

consola.log('-------------------------------------------');
consola.log('------ Bienvenido a la calculadora -------');
consola.log('-------------------------------------------');

function main(){
   ask().then(response => {
      try {
         if(!response) throw new Error('Ok, vuelve pronto...');
         consola.start('Comenzemos!');
         menu();
         main();
      } catch (error) {
         consola.error(error.message)
      }
   })
}

main();





