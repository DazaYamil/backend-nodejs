import fs from 'fs';
const {pathname: root} = new URL('../model', import.meta.url)

let logs = [];

try {
   const data = fs.readFileSync(root + '/logs.txt', 'utf8');
   // console.log(data);
   logs = Array.isArray(JSON.parse(data))? JSON.parse(data) : [];
} catch (error) {
   console.log(error.message);
}

function registrar(n1,n2,result,op){
   logs.push({operacion: `${n1} ${op} ${n2} = ${result}`});
   fs.writeFileSync(root + '/logs.txt', JSON.stringify(logs));
}

export default registrar;

