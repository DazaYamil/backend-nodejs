import registrar from '../model/data.js';
//Mirar como pasar la funcion registrar como callback

const sumar = (n1,n2) => {
   let result = n1 + n2;
   console.log('Result: ',result);
   registrar(n1,n2,result,'+');
};

const restar = (n1, n2) => {
   let result = n1 - n2;
   console.log('Result: ',result);
   registrar(n1,n2,result,'-');
};

const multiplicar = (n1, n2) => {
   let result = n1 * n2;
   console.log('Result: ',result);
   registrar(n1,n2,result,'*');
};

const dividir = (n1, n2) => {
   let result = n1 / n2;
   console.log('Result: ',result);
   registrar(n1,n2,result,'/');

};

export { sumar, restar, multiplicar, dividir };