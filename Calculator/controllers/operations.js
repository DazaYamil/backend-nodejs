import { sumar, restar, multiplicar, dividir} from '../services/mainOperations.js';

function checkData(opt, n1, n2){
   console.log();
   if(!(opt >= 1 && opt <= 4)){
      console.log(' ****** Error: Debe ingresar un valor entre 1 y 4 para realizar operaciones ******');
      return true
   }
   if(!(n1 >= 1 && n2 >= 1)){
      console.log('*** Error: Debe ingresar valores numericos mayores a 1 ****')
      return true
   }
   return false;
}

const controller = (optionUser, num1, num2) => {
   try {
      if(checkData(optionUser, num1, num2)) throw new Error('Incorrect Data');
      
      switch (optionUser) {
         case 1:
            sumar(num1, num2);
            break;
         case 2:
            restar(num1, num2);
            break;
         case 3:
            multiplicar(num1, num2);
            break;
         case 4:
            dividir(num1, num2);
            break;
      } 

   } catch (error) {
      console.error('-------------------------------');
      console.error(`------- ${error.message} --------`);
      console.error('-------------------------------');
   }
}

export default controller;