/* 
Enunciado: Realizar el ejericio de FIZZ BUZZ con promesas.
Toda validación de cada número debe ser realizado por una función que retorne una promesa. Validaciones: 
# Sí es múltiplo de 3 debe decir fizz
# Sí es múltiplo de 5 debe decir buzz
# Sí es múltiplo de 3 y 5 debe decir fizzbuzz
# Sí no cumple con ninguna de las condiciones anteriores debe mostrar el número 
*/

//* Prueba -----------------------------------------------------------------
/* const promises = [];

function validar(nro) {
   return new Promise((resolve, reject) => {
      if(nro % 3 === 0 && nro % 5 === 0){
         resolve('FizzBuzz')
      }
   })
}

for (let i = 0; i < 5; i++) {
   console.log(i);
   promises.push(validar(i));
}

Promise.allSettled(promises).then((resp) => {
   resp.forEach((item) => console.log(item.value));
}); */


//* Codigo: -----------------------------------------------------------------
const promises = [];
let initialValue = process.argv[2];
let finalValue = process.argv[3];
//* Function callback
function validateNumber(nro, result){
   return new Promise((resolve, reject) => {
      try {
         // let result = ''; CONSULTAR ????
         result = '';
         if(nro % 3 === 0) result += 'Fizz';
         if(nro % 5 === 0) result += 'Buzz';
         //* Si el nro pasado por parametro no es multiplo de 3 o/y 5, retorna el numero.
         //* Probar Settimeout.
         resolve(result || nro);
      } catch (error) {
         console.log(error);
      }
   })
}

//* Function 1
function fizzbuzz(initial, final, FNvalidateNumber) {
   //Validar...
   let result;
   for (let i = initial; i <= final; i++) {
      promises.push(FNvalidateNumber(i, result));
   }
}

//* Function 2
function showResult(promises){
   Promise.allSettled(promises)
      .then(response => {
         response.forEach(data => {
            console.log(data.value);
         }) 
      })
}

fizzbuzz(initialValue, finalValue, validateNumber); //* Function 1
showResult(promises); //* Function 2

