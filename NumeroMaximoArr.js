/* Mini desafío
Crea un array con 10 elementos numéricos y sobre ese array dime cual es el valor más alto
Nota: No busques en internet la solución, aprovecha el ejercicio para ejercitar tu lógica, con un array, una variable y un ciclo for lo puedes resolver. */


function buscarNumeroMaximo(arr) {
   let dimensionArray = arr.length;
   let numMax = arr[0];
   
   for (let i = 1; i < dimensionArray; i++) {
      if (arr[i] > numMax) {
         numMax = arr[i];
      }
   }
   return numMax;
}

const arrayNumeros = [82, 12, 98, 102, 234, 56, 19, 57, 78, 3];
const numeroMaximo = buscarNumeroMaximo(arrayNumeros);
console.log(numeroMaximo);
