import colors from 'colors';;
import fetch from 'node-fetch';
const URL_API = 'https://jsonplaceholder.typicode.com/users';

const generateAge = (min = 1, max = 100) => Math.floor(Math.random() * (max - min))
const showUsers = (arr) => {
   let index = 1;
   arr.forEach(dato => {
      let {id, name, username, email, age, address: {city}, company: {name : nameCompany} } = dato;

      //*Consultar la forma de iterar sobre las destructuracion del objeto 'dato'
      console.log(` * User ${index} *`);
      console.log(`· ID: ${id}`);
      console.log(`· Full Name: ${name} ${username}`);
      console.log(`· Email: ${email}`);
      console.log(`· Age: ${age}`);
      console.log(`· City and Company: ${city} - ${nameCompany}`);
      console.log(``);
      index++;
   })
}

function consumirApi(data){
   data.forEach(user => user.age = generateAge(1, 100));
   const approvedUsers = data.filter(user => user.age >= 18);
   const rejectedUsers = data.filter(user => user.age <= 17);

   console.log(` ****** APPROVED USERS "18 years or more" - USER ${arr.length}: ****** `.blue) 
   approvedUsers.length === 0 ? console.log('No approved user added') : showUsers(approvedUsers); 
   console.log(` ***** REJECTED USERS "17 years or less" - USER ${arr.length}: ****** `.red);
   rejectedUsers.length === 0 ? console.log('No rejected user added') : showUsers(rejectedUsers); 
};

async function main(url){
   try {
      const response = await fetch(url);
      if (response.ok){
         const data = await response.json();
         consumirApi(data);
      } else {
         throw new Error('Error en la solicitud');
      }
   } catch (error) {
      console.log(`${error.message}`);
   }
   
}

console.log(' -----------------------------------------------');
console.log(' ------ Hello, Welcome. Starting process -------'.green);
console.log(' -----------------------------------------------');
main(URL_API);

