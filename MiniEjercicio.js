/* Mini Ejercicio
Trabajemos con el último enunciado para diseñar una aplicación
El juego consiste en que el usuario debe adivinar un número entre 0 y 100
Cada vez que el usuario ingresa un número la aplicación debe indicar

El número {numero_ingresado} es mayor al que debes adivinar
El número {numero_ingresado} es menor al que debes adivinar
Ganaste: El número era {numero_ingresado} y lo hiciste en { } intentos */

const prompt = require("prompt-sync")();

const NRO_INTENTOS = 5;
const NRO_MAQUINA = Math.floor(Math.random() * (100 - 1) + 1);
console.log(NRO_MAQUINA);

console.log(" ===  ¡Bienvenido al Juego! Adivina el número entre 1 y 100. Tienes 5 intentos. ===");
console.log(""); //Salto de linea

let intentos = 0;
let nroUsuario;
let seguir = false;

do {
   nroUsuario = Number(prompt("Ingresa Un número: "));
   intentos++;

   if (nroUsuario === NRO_MAQUINA) {
      console.log(` === ✅ GANASTE !!! Adivinaste el número en ${intentos} intentos. Nro maquina ${NRO_MAQUINA} - Nro Ingresado ${nroUsuario}. Felicidades !!! `);
      seguir = true;
   } else {
      nroUsuario > NRO_MAQUINA
         ? console.log(`❌ El número ${nroUsuario} es mayor al que debes adivinar`)
         : console.log(`❌ El número ${nroUsuario} es menor al que debes adivinar`);

      console.log(""); //Salto de linea
      console.log(`******* Intento nro ${intentos} - Te quedan ${NRO_INTENTOS - intentos} intentos **********`);
      console.log("");
   }
} while (!seguir && intentos < NRO_INTENTOS);

if (intentos === NRO_INTENTOS) {
   console.log(
      `Perdiste :( Llegaste a la cantidad máxima de intentos. No te rindas, Vuelve a intentarlo!!!`
   );
}
