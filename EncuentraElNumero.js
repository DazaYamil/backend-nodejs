// En el rango de 0 a 10000 la idea es que la máquina encuentre un número ingresado por el usuario desde la consola en la menor cantidad de intentos posibles

console.log(" ===  ¡Bienvenido al Juego! Ingresa un número y la máquina intentará adivinar tu número ingresado en la menor cantidad de intentos posibles. ===");
console.log(""); //Salto de linea
const prompt = require("prompt-sync")();

const NRO_USUARIO = Number(prompt('Ingresa un numero: '));
const NRO_INTENTOS_MAXIMO = 10;

let nroMaquina;
let cantidadDeIntentos = 0;
let seguir = false;

if(NRO_USUARIO >= 0 & NRO_USUARIO <= 10000){
   do {
      nroMaquina = Math.floor(Math.random() * (10000 - 0) + 0);
      cantidadDeIntentos++;
      
      /* if(cantidadDeIntentos === Math.floor(Math.random() * (10 - 1) + 1)){
         nroMaquina = NRO_USUARIO;
      } */
   
      if (nroMaquina === NRO_USUARIO) {
         console.log(` === ✅ Encontre tu numero ingresado "${NRO_USUARIO}" en ${cantidadDeIntentos} intentos === `);
         seguir = true;
      } else {
         console.log(`❌ El numero ${nroMaquina} no es correcto...`);
      }
   } while (!seguir && cantidadDeIntentos < NRO_INTENTOS_MAXIMO);
}else{
   console.log('El numero ingresado deber ser mayor o igual a 0 y menor o igual a 10000');
}


if (cantidadDeIntentos === NRO_INTENTOS_MAXIMO) {
   console.log('');
   console.log(`****** La maquina llego a la cantidad maxima de intentos :( *******`);
}

